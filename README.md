# PyMake #

PyMake is a make-like tool written in and consumed by Python.

Documentation:

[Initial project pre-proposal](https://docs.google.com/document/d/1g-4q0m2gVThCtRLfDQDLSD9lG-8zeJ6U5iYNG4Shm-o/edit?usp=sharing)

[Initial project proposal](https://docs.google.com/document/d/1xOiD-zI6tzsKfPtesDU84HfXzghjICkFG9bFhKxuuOk/edit?usp=sharing)

[Design document](https://docs.google.com/document/d/1c9owAbKXjItxbXh0GV2ZgjCJCxEaefbcJqB3Fyg6J6c/edit?usp=sharing)