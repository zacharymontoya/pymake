import unittest
import os
import sys
sys.path.append("..")

from task import *

def create_file(path):
    if os.path.exists(path):
        os.utime(path, None)
    else:
        file(path, 'w')

@task()
def dependent_task():
    pass

@task(["nonexistent_task_dependency_output.txt"], [dependent_task])
def nonexistent_output_file_with_task_dependency_task():
    pass

@task(["preexisting_task_dependency_output.txt"], [dependent_task])
def preexisting_output_file_with_task_dependency_task():
    pass

class BothDependenciesTestCase(unittest.TestCase):
    def test_nonexistent_output_file_with_task_dependency_task(self):
        if os.path.exists("nonexistent_task_dependency_output.txt"):
            os.remove("nonexistent_task_dependency_output.txt")

        self.assertFalse(os.path.exists("nonexistent_task_dependency_output.txt"))
        nonexistent_output_file_task_obj = task.get_task("nonexistent_output_file_with_task_dependency_task")
        self.assertFalse(nonexistent_output_file_task_obj.triggered)

        nonexistent_output_file_task_obj.invoke()
        self.assertTrue(nonexistent_output_file_task_obj.triggered)

    def test_preexisting_output_file_with_task_dependency_task(self):
        create_file("preexisting_task_dependency_output.txt")
        self.assertTrue(os.path.exists("preexisting_task_dependency_output.txt"))
        preexisting_output_file_task_obj = task.get_task("preexisting_output_file_with_task_dependency_task")
        self.assertFalse(preexisting_output_file_task_obj.triggered)

        preexisting_output_file_task_obj.invoke()
        self.assertTrue(preexisting_output_file_task_obj.triggered)
        os.remove("preexisting_task_dependency_output.txt")