import unittest
import os
import sys
sys.path.append("..")

from task import *

intermediate_file_path = "intermediate_file.txt"
output_file_path = "output_file.txt"

CLEAN.append(intermediate_file_path)
CLOBBER.append(output_file_path)

def create_file(path):
    if os.path.exists(path):
        os.utime(path, None)
    else:
        file(path, 'w')

@task(outputs=["preexisting_file.txt"])
def preexisting_file_task():
    pass

class BuiltinTasksTestCase(unittest.TestCase):
    def test_clean_and_clobber_task(self):
        create_file("intermediate_file.txt")
        create_file("output_file.txt")

        self.assertTrue(os.path.exists(intermediate_file_path))
        self.assertTrue(os.path.exists(output_file_path))

        task.get_task("clobber").invoke()
        self.assertTrue(task.get_task("clean").triggered)
        self.assertTrue(task.get_task("clobber").triggered)

        self.assertFalse(os.path.exists(intermediate_file_path))
        self.assertFalse(os.path.exists(output_file_path))