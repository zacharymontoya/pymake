import unittest
from test_builtin_tasks import *
from test_task_dependencies import *
from test_file_dependencies import *
from test_both_dependencies import *

if __name__ == '__main__':
    unittest.main()
