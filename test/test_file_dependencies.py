import unittest
import os
import sys
sys.path.append("..")

from task import *

def create_file(path):
    if os.path.exists(path):
        os.utime(path, None)
    else:
        file(path, 'w')

@task(outputs=["preexisting_output_file.txt"])
def preexisting_output_file_task():
    pass

@task(outputs=["nonexistent_output_file.txt"])
def nonexistent_output_file_task():
    pass

@task(outputs=["older_output.txt"], dependencies=["older_input.txt"])
def older_output_file_task():
    pass

@task(outputs=["newer_output.txt"], dependencies=["newer_input.txt"])
def newer_output_file_task():
    pass

@task(outputs=["new_output.txt"], dependencies=["new_output_existing_input.txt"])
def new_output_with_existing_input():
    pass

class FileDependenciesTestCase(unittest.TestCase):
    def test_preexisting_output_file_task(self):
        create_file("preexisting_output_file.txt")
        self.assertTrue(os.path.exists("preexisting_output_file.txt"))
        preexisting_output_file_task_obj = task.get_task("preexisting_output_file_task")
        self.assertFalse(preexisting_output_file_task_obj.triggered)

        preexisting_output_file_task_obj.invoke()
        self.assertFalse(preexisting_output_file_task_obj.triggered)
        os.remove("preexisting_output_file.txt")

    def test_nonexistent_output_file_task(self):
        if os.path.exists("nonexistent_output_file.txt"):
            os.remove("nonexistent_output_file.txt")

        self.assertFalse(os.path.exists("nonexistent_output_file.txt"))
        nonexistent_output_file_task_obj = task.get_task("nonexistent_output_file_task")
        self.assertFalse(nonexistent_output_file_task_obj.triggered)

        nonexistent_output_file_task.invoke()
        self.assertTrue(nonexistent_output_file_task_obj.triggered)

    def test_older_output_file_task(self):
        # Remove file dependencies
        if os.path.exists("older_input.txt"):
            os.remove("older_input.txt")
        if os.path.exists("older_output.txt"):
            os.remove("older_output.txt")

        # Create input and output files
        # The output file will be created first so it is "older" and by
        # definition the task will need to run
        create_file("older_output.txt")
        create_file("older_input.txt")
        self.assertTrue(os.path.getmtime("older_input.txt") > os.path.getmtime("older_output.txt"))

        older_output_file_task_obj = task.get_task("older_output_file_task")
        self.assertFalse(older_output_file_task_obj.triggered)
        older_output_file_task_obj.invoke()
        self.assertTrue(older_output_file_task_obj.triggered)

        os.remove("older_input.txt")
        os.remove("older_output.txt")

    def test_newer_output_file_task(self):
        # Remove file dependencies
        if os.path.exists("newer_input.txt"):
            os.remove("newer_input.txt")
        if os.path.exists("newer_output.txt"):
            os.remove("newer_output.txt")

        # Create input and output files
        # The output file will be created second so it is "newer" and by
        # definition the task will not need to run
        create_file("newer_input.txt")
        create_file("newer_output.txt")
        self.assertTrue(os.path.getmtime("newer_output.txt") > os.path.getmtime("newer_input.txt"))

        newer_output_file_task_obj = task.get_task("newer_output_file_task")
        self.assertFalse(newer_output_file_task_obj.triggered)
        newer_output_file_task_obj.invoke()
        self.assertFalse(newer_output_file_task_obj.triggered)

        os.remove("newer_input.txt")
        os.remove("newer_output.txt")

    def test_new_output_with_existing_input(self):
        if os.path.exists("new_output.txt"):
            os.remove("new_output.txt")
        create_file("new_output_existing_input.txt")
        self.assertFalse(os.path.exists("new_output.txt"))
        self.assertTrue(os.path.exists("new_output_existing_input.txt"))

        new_output_with_existing_input_obj = task.get_task("new_output_with_existing_input")
        self.assertFalse(new_output_with_existing_input_obj.triggered)
        new_output_with_existing_input_obj.invoke()
        self.assertTrue(new_output_with_existing_input_obj.triggered)

        os.remove("new_output_existing_input.txt")
