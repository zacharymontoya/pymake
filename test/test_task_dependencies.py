import unittest
import sys
sys.path.append("..")

from task import *

@task()
def static_dependency_task():
    pass

@task(dependencies=[static_dependency_task])
def task_with_static_dependency():
    pass

@task()
def dynamic_dependency_task():
    pass

@task()
def task_with_dynamic_dependency():
    pass

@task()
def task_with_no_dependencies():
    pass

class TaskDependenciesTestCase(unittest.TestCase):
    def test_nonexistent_task(self):
        self.assertRaises(SystemExit, task.get_task, "nonexistent_task")

    def test_task_with_no_dependencies(self):
        task_with_no_dependencies_obj = task.get_task("task_with_no_dependencies")
        self.assertFalse(task_with_no_dependencies_obj.triggered)

        task_with_no_dependencies_obj.invoke()

        self.assertTrue(task_with_no_dependencies_obj.triggered)

    def test_static_dependency(self):
        task_with_static_dependency_obj = task.get_task("task_with_static_dependency")
        static_dependency_task_obj = task.get_task("static_dependency_task")

        # Before invoking `task_with_static_dependency`, `static_dependency_task` has not been triggered
        self.assertFalse(task_with_static_dependency_obj.triggered)
        self.assertFalse(static_dependency_task_obj.triggered)

        # Invoke `task_with_static_dependency`
        task_with_static_dependency_obj.invoke()
        self.assertTrue(task_with_static_dependency_obj.triggered)
        self.assertTrue(static_dependency_task_obj.triggered)

    def test_dynamic_dependency(self):
        task_with_dynamic_dependency_obj = task.get_task("task_with_dynamic_dependency")
        dynamic_dependency_task_obj = task.get_task("dynamic_dependency_task")
        task_with_dynamic_dependency_obj.dependencies.append(dynamic_dependency_task_obj)

        # Before invoking `task_with_dynamic_dependency`, `dynamic_dependency` has not been triggered
        self.assertFalse(task_with_dynamic_dependency_obj.triggered)
        self.assertFalse(dynamic_dependency_task_obj.triggered)

        # Invoke `task_with_dynamic_dependency`
        task_with_dynamic_dependency_obj.invoke()
        self.assertTrue(task_with_dynamic_dependency_obj.triggered)
        self.assertTrue(dynamic_dependency_task_obj.triggered)
