import glob
import os
import warnings

class task(object):
    TASKS = {}

    class _DependencyObject(object):
        def needs_to_run(self):
            assert False, "Subclass must implement needs_to_run"

        def invoke(self):
            assert False, "Subclass must implement invoke"

    class _FileDependency(_DependencyObject):
        def __init__(self, path):
            self.path = path
            self.triggered = False

        def raise_error_if_path_does_not_exist(self):
            if not os.path.isfile(self.path):
                raise SystemExit("error: file '%s' specified by file dependency does not exist" % self.path)

        def needs_to_run(self):
            pass

        def invoke(self):
            self.raise_error_if_path_does_not_exist()

    class _TaskObject(_DependencyObject):
        def needs_to_run(self):
            if self.triggered:
                return False

            # No dependencies or outputs specified. This needs to run.
            if len(self.outputs) == 0 and len(self.dependencies) == 0:
                return True
            # No outputs specified but dependencies specified.
            elif len(self.outputs) == 0:
                # This needs to run if any dependencies have run.
                if True in [dep.triggered for dep in self.dependencies]:
                    return True
            # No dependencies specified but outputs specified.
            elif len(self.dependencies) == 0:
                # This needs to run if any outputs do not exist.
                if False in [os.path.isfile(out) for out in self.outputs]:
                    return True
            # Both dependencies and outputs specified.
            else:
                # This needs to run if any dependencies have run.
                if True in [dep.triggered for dep in self.dependencies]:
                    return True

                # This needs to run if any outputs do not exist.
                if False in [os.path.isfile(out) for out in self.outputs]:
                    return True

                # This needs to run if any file dependencies are newer than the output files
                file_dependencies_modified_times = [os.path.getmtime(dep.path) for dep in self.dependencies if isinstance(dep, task._FileDependency)]
                output_modified_times = [os.path.getmtime(output) for output in self.outputs]
                if file_dependencies_modified_times and max(file_dependencies_modified_times) > min(output_modified_times):
                    return True

            # If none of the cases returned True, then return False
            return False

        def invoke(self):
            if self.dependencies:
                for dependent_task in self.dependencies:
                    dependent_task.invoke()

            if self.needs_to_run():
                self.action()
                self.triggered = True

    def __init__(self, outputs=None, dependencies=None):
        self._task = task._TaskObject()
        self._task.outputs = outputs if outputs else []
        self._task.dependencies = dependencies if dependencies else []
        self._task.triggered = False

        self._task.dependencies = []
        if dependencies:
            for dependency in dependencies:
                if isinstance(dependency, task._TaskObject):
                    self._task.dependencies.append(dependency)
                elif isinstance(dependency, str):
                    self._task.dependencies.append(task._FileDependency(dependency))

    def __call__(self, fn):
        self._task.name = fn.__name__
        self._task.action = fn
        task.register_task(self._task)
        return self._task # once the decorator exits, return the underlying _TaskObject so other task() calls can correctly depend on this

    @staticmethod
    def register_task(new_task):
        task.TASKS[new_task.name] = new_task

    @staticmethod
    def get_task(name):
        if name in task.TASKS:
            return task.TASKS[name]
        else:
            raise SystemExit("error: No task recognized with name '%s'" % name)

@task()
def default():
    print "default -- the default task has not been overwritten"

CLEAN=[]
CLOBBER=[]

def delete_globs(glob_list):
    delete_files = []
    for file_glob in glob_list:
        delete_files.extend(glob.glob(file_glob))
    for delete_file in delete_files:
        if os.path.isfile(delete_file):
            os.remove(delete_file)
        else:
            warnings.warn("File '%s' does not exist. Unable to delete." % delete_file)

@task()
def clean():
    delete_globs(CLEAN)

@task(dependencies=[clean])
def clobber():
    delete_globs(CLOBBER)
