#!/usr/bin/env python
import glob
import imp
import os
import sys

from task import *

# Use execfile to execute each .pymake file in this directory.
#
# Each .pymake file is a python script with some functions decorated with
# the task decorator. To evaluate them as part of the main application,
# we use execfile and bring the functions into the global namespace.
pymake_files = glob.glob(os.path.join(os.getcwd(),'*.pymake'))
for pymake_file in pymake_files:
    execfile(pymake_file)

if __name__ == "__main__":
    tasknames = sys.argv[1:]
    if not tasknames:
        tasknames.append("default")

    for taskname in tasknames:
        task.get_task(taskname).invoke()
